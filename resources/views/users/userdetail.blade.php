@extends('base')

@section('main') 

    <div class="main-content-inner">
        <div class="row">
            <div class="col-lg-12 col-ml-12">
                <div class="row mt-5">
                    <div class="col-12">
                        <h1 class="text-center">User Details</h1>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group col-md-6 pull-left">
                            <label for="example-text-input-lg" class="col-form-label">First Name</label>
                            <input class="form-control form-control-lg" type="text" placeholder="First Name" id="firstNameUd"  Value = {{ $userFound[0]->first_name}}>
                        </div>
                        <div class="form-group col-md-6 pull-left">
                            <label for="example-text-input-lg" class="col-form-label">Last Name</label>
                            <input class="form-control form-control-lg" type="text" placeholder="Last Name" id="lastNameUd" Value= {{ $userFound[0]->last_name}}>
                        </div>
                    </div>
                    <div class="col-12">
                    
                    </div>

                </div>
                <div class="mt-5">
                    <div class="row">

                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="single-table">
                                        <div class="table-responsive">
                                            <table class="table table-striped text-center">
                                                <thead class="text-uppercase">
                                                    <tr class="text-white bg-primary">
                                                        <th scope="col" style="text-align: left; width: 60%">la Pizza Name</th>
                                                        <th scope="col">Assigned On</th>
                                                        <th scope="col">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(count($allUserDetails) > 0)
                                                        @foreach($allUserDetails as $userDetail)
                                                        <tr>
                                                            <td style="text-align: left">{{$userDetail->pizzas->name}}</td>
                                                            <td>{{$userDetail->created_at}}</td>
                                                            <td>
                                                                <a href="#" data-toggle="modal" data-target="#un-assign-pizza" id="btnEat"><i class="ti-apple"></i> Eat</a>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                            </div>
                                <button type="button" class="btn btn-primary btn-lg mt-5 pull-right" data-toggle="modal" data-target="#add-pizza">Give a Pizza</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="un-assign-pizza">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eat Pizza</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <p style="text-align: center"> Do you want this pizza to be eaten by this user?</p>
                    <br>
                    <h3 style="text-align: center" id="pizzaName"></h3>
                    <br>
                    <h5 style="text-align: center">{{ $userFound[0]->first_name}}  {{ $userFound[0]->last_name}}</h5>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Eat</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="add-pizza">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Give a Pizza</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <p style="text-align: center">Give an available pizza from the db to this user</p>
                    <div class="form-group">
                        <label class="col-form-label">Pizzas</label>
                        <select class="custom-select form-control-lg" style="font-size: 13px" onchange="showDiv(this)" id="selectPizzas">
                        @if(count($allPizzasName) > 0)
                            @foreach($allPizzasName as  $key)
                            <option value={{ $key->id}}>{{$key->name}}</option>
                            @endforeach
                        @endif
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id ="btnGivePizza">Give That Pizza!</button>
                </div>
            </div>
        </div>
    </div>
@stop 


@section('scripts')
    
@stop 

