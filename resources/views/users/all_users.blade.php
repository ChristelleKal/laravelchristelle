@extends('base')

@section('main')          
    <div class="row">
        <div class="col-lg-12 col-ml-12">
            <div class="row mt-5">
                <div class="col-12">
                    <h1 class="text-center">All Users </h1>
                </div>
                <div class="col-12">
                    <div class="card mt-3">
                        <div class="card-body">
                            <h4 class="header-title">Filters</h4>
                            <form class="needs-validation" novalidate="">
                                <div class="form-row">

                                    <div class="col-md-10 mb-3">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control form-control-lg" placeholder="Search..." id="searchUserValue">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2"><i class="ti-search"></i></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2 mb-3">
                                        <button class="btn btn-primary btn-block form-control-lg" type="submit" id="searchBtn">Apply</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-5">
                <div class="row">

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="single-table">
                                    <div class="table-responsive">
                                        <table class="table table-striped text-center">
                                            <thead class="text-uppercase">
                                                <tr class="text-white bg-primary">
                                                    <th scope="col" style="width:50%; text-align: left">User</th>
                                                    <th scope="col" style="width:30%; text-align: center">Joined On</th>
                                                    <th scope="col" style="width:20%;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 @foreach($allUsers as $user)
                                                <tr>
                                                    <td class ="d-none">{{$user->id}} </td>
                                                    <td style="text-align: left">{{$user->first_name}}   {{$user->last_name}}</td>
                                                    <td style="text-align: center">{{$user->created_at}}</td>
                                                    <td>
                                                        <a href="{{url('userinfo/'.$user->id)}}" style="margin-right: 4vh"><i class="ti-eye"></i> View</a>
                                                        <a href="{{url('users/'.$user->id)}}" data-toggle="modal"  data-target="#delete-user" id="deleteUserRef" ><i class="ti-trash"></i> Delete <span class="d-none">{{$user->first_name}}   {{$user->last_name}}</span></a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                             </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>

                        </div>
                        
                        <button type="button" class="btn btn-primary btn-lg mt-5 " data-toggle="modal" data-target="#add-user">Add User</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
  
    <div class="modal fade" id="add-user">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add User</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="example-text-input-lg" class="col-form-label">First Name</label>
                        <input class="form-control form-control-lg" type="text" placeholder="First Name" id="firstName" name = "firstName">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input-lg" class="col-form-label">Last Name</label>
                        <input class="form-control form-control-lg" type="text" placeholder="Last Name" id="lastName" name="lastName">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveUser" name="saveUser">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-user">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete User</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <p style="text-align: center"> Do you want to delete the following user?</p>
                    <br>
                    <h3 style="text-align: center" id = "displayUserName"> </h3>
                    <span class="d-none" id="deleteUserUri"> <span>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="deleteUser">Delete</button>
                </div>
            </div>
        </div>
    </div>


@stop

@section('scripts')

@stop