<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pizzas extends Model
{
    protected $fillable = [
        'name',
        'pizza_id'     
    ];


    /**
     * Get the Pizzas related to the user
     */
    public function pizzas()
    {
        return $this->hasMany('App\user_pizzas', 'pizza_id', 'id');
    }
}
