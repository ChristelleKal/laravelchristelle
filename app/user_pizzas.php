<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_pizzas extends Model
{
    protected $fillable = [
        'user_id',
        'pizza_id',
        'created_at',                
    ];

    /**
     * Get the Pizzas related to the user
     */
    public function pizzas()
    {
        return $this->belongsTo('App\pizzas', 'pizza_id');
    }

    public function users()
    {
        return $this->belongsTo('App\users', 'user_id');
    }
}
