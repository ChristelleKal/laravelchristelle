<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'created_at', 
        'user_id'               
    ];

    /**
     * Get the Pizzas related to the user
     */
    public function userPizzas()
    {
        return $this->hasMany('App\user_pizzas', 'user_id', 'id');
    }
}
