<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\users;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUsers = users::all();
        return view('users.all_users', compact('allUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.all_users');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //validate the request
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
        ]);

        //create a new user
        $user = new users;
       // set  First name and last name
        $user->first_name = $request->get('first_name');
        $user->last_name =$request->get('last_name');
        // save user
        $user->save();
        return redirect('/users')->with('success', 'User saved!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = users::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = users::find($id);
        return view('users.all_users', compact('user')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
        ]);

        $user = users::find($id);
        $user->first_name =  $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->save();
        return view('users.all_users', compact('user')); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = users::find($id);
        $user->delete();

        return redirect('/users')->with('success', 'User deleted!');
    }
    /**
     * Controller to handle user search
     */
    public function search(Request $request, $name)
    {
        $userFound = users::where('first_name', $name)
                    ->orWhere('last_name', $name)
                    ->get();
        return response()->json($userFound);
    }
}
