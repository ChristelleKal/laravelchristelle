<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user_pizzas;
use App\pizzas;
use App\users;

class UserDetailController extends Controller
{

   

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($id);
        /*// get the user details , loaddin user details and pizzas details
        $allUserDetails = user_pizzas::where('user_id', $id)->with(['users', 'pizzas'])->get();
        
        //Get pizza value for  select option
        $allPizzasName = pizzas::all();
        // display user details view 
        return view('users.userdetail', compact('allUserDetails'), compact('allPizzasName'));
        */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {  
        return  redirect('/userdetail');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate the request
        $request->validate([  
            'first_name'=>'required',
            'last_name'=>'required',
            'pizzaId'=>'required',
        ]);
        
        // set user details data
        $firstNameUser =  $request->get('first_name');
        $lastNameUser =  $request->get('last_name');
        $pizzaId = $request->get('pizzaId');
        //check if user exists in db
        $user = users::where('first_name', $firstNameUser)
                    ->where('last_name', $lastNameUser)
                    ->first();

        // if user does not  exist in db, give user pizza in table user_pizzas
        if ($user === null){
            //create a new user
            $user = new users;
            // set  First name and last name
            $user->first_name =$firstNameUser;
            $user->last_name =$lastNameUser;
            // save user
            $user->save();
        }
        //Create a new user pizzas details
        $newUserPizza = new user_pizzas;
        $newUserPizza->user_id =  $user->id;
        $newUserPizza->pizza_id = $pizzaId;
        $newUserPizza->save();
        
        //return redirect('/userinfo', [$user->id])->with('success', 'user Pizzas detail created');
        return response()->json($user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   // pass id to index page for now
        // redirect to index page
        return  redirect('/userdetail'.$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function userinfo($userId)
    {
        // get user  first
        $userFound = users::where('id', $userId)->get();
        
        //return response()->json($userFound);
        // when user is non existant return page not found
        if($userFound->isEmpty()){
             return  abort(404, 'User not found');
        }
        // get the user details , loaddin user details and pizzas details
        $allUserDetails = user_pizzas::where('user_id', $userId)->with(['users', 'pizzas'])->get();
        $allUserDetails =$allUserDetails?: '';
        //Get pizza value for  select option
        $allPizzasName = pizzas::all();

        // display user details view 
        //return view('users.userdetail',compact('userFound'), compact('allUserDetails'), compact('allPizzasName'));
        return view('users.userdetail',compact('userFound','allUserDetails','allPizzasName'));

        
        
    }
}
