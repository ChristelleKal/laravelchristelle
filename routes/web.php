<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('users.userdetail');
});
// Default user routes
Route::resource('users', 'UserController');

//Default userDetails routes
Route::resource('userdetail', 'UserDetailController');

Route::get('/userinfo/{userId}', 'UserDetailController@userinfo');

//Route to search for a user
Route::get('/search/{name}','UserController@search');