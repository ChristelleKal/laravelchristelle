(function($) {
    "use strict";

    /*================================
    Preloader
    ==================================*/

    var preloader = $('#preloader');
    $(window).on('load', function() {
        preloader.fadeOut('slow', function() {
            $(this).remove();
        });
    });

    /*================================
    sidebar collapsing
    ==================================*/
    $('.nav-btn').on('click', function() {
        $('.page-container').toggleClass('sbar_collapsed');
    });

    /*================================
    Start Footer resizer
    ==================================*/
    var e = function() {
        var e = (window.innerHeight > 0 ? window.innerHeight : this.screen.height) - 5;
        (e -= 67) < 1 && (e = 1), e > 67 && $(".main-content").css("min-height", e + "px")
    };
    $(window).ready(e), $(window).on("resize", e);

    /*================================
    sidebar menu
    ==================================*/
    $("#menu").metisMenu();

    /*================================
    slimscroll activation
    ==================================*/
    $('.menu-inner').slimScroll({
        height: 'auto'
    });
    $('.nofity-list').slimScroll({
        height: '435px'
    });
    $('.timeline-area').slimScroll({
        height: '500px'
    });
    $('.recent-activity').slimScroll({
        height: 'calc(100vh - 114px)'
    });
    $('.settings-list').slimScroll({
        height: 'calc(100vh - 158px)'
    });

    /*================================
    stickey Header
    ==================================*/
    $(window).on('scroll', function() {
        var scroll = $(window).scrollTop(),
            mainHeader = $('#sticky-header'),
            mainHeaderHeight = mainHeader.innerHeight();

        // console.log(mainHeader.innerHeight());
        if (scroll > 1) {
            $("#sticky-header").addClass("sticky-menu");
        } else {
            $("#sticky-header").removeClass("sticky-menu");
        }
    });

    /*================================
    form bootstrap validation
    ==================================*/
    $('[data-toggle="popover"]').popover()

    /*------------- Start form Validation -------------*/
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);

    /*================================
    datatable active
    ==================================*/
    if ($('#dataTable').length) {
        $('#dataTable').DataTable({
            responsive: true
        });
    }
    if ($('#dataTable2').length) {
        $('#dataTable2').DataTable({
            responsive: true
        });
    }
    if ($('#dataTable3').length) {
        $('#dataTable3').DataTable({
            responsive: true
        });
    }


    /*================================
    Slicknav mobile menu
    ==================================*/
    $('ul#nav_menu').slicknav({
        prependTo: "#mobile_menu"
    });

    /*================================
    login form
    ==================================*/
    $('.form-gp input').on('focus', function() {
        $(this).parent('.form-gp').addClass('focused');
    });
    $('.form-gp input').on('focusout', function() {
        if ($(this).val().length === 0) {
            $(this).parent('.form-gp').removeClass('focused');
        }
    });

    /*================================
    slider-area background setting
    ==================================*/
    $('.settings-btn, .offset-close').on('click', function() {
        $('.offset-area').toggleClass('show_hide');
        $('.settings-btn').toggleClass('active');
    });

    /*================================
    Owl Carousel
    ==================================*/
    function slider_area() {
        var owl = $('.testimonial-carousel').owlCarousel({
            margin: 50,
            loop: true,
            autoplay: false,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1
                },
                450: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1000: {
                    items: 2
                },
                1360: {
                    items: 1
                },
                1600: {
                    items: 2
                }
            }
        });
    }
    slider_area();

    /*================================
    Fullscreen Page
    ==================================*/

    if ($('#full-view').length) {

        var requestFullscreen = function(ele) {
            if (ele.requestFullscreen) {
                ele.requestFullscreen();
            } else if (ele.webkitRequestFullscreen) {
                ele.webkitRequestFullscreen();
            } else if (ele.mozRequestFullScreen) {
                ele.mozRequestFullScreen();
            } else if (ele.msRequestFullscreen) {
                ele.msRequestFullscreen();
            } else {
                console.log('Fullscreen API is not supported.');
            }
        };

        var exitFullscreen = function() {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else {
                console.log('Fullscreen API is not supported.');
            }
        };

        var fsDocButton = document.getElementById('full-view');
        var fsExitDocButton = document.getElementById('full-view-exit');

        fsDocButton.addEventListener('click', function(e) {
            e.preventDefault();
            requestFullscreen(document.documentElement);
            $('body').addClass('expanded');
        });

        fsExitDocButton.addEventListener('click', function(e) {
            e.preventDefault();
            exitFullscreen();
            $('body').removeClass('expanded');
        });
    }
       $('#datepickerform').datepicker();
       $('#datepicker').on('changeDate', function() {
    $('#my_hidden_input').val(
        $('#datepicker').datepicker('getFormattedDate')

    );
})
          $('#datepickerto').datepicker();
       $('#datepicker').on('changeDate', function() {
    $('#my_hidden_input').val(
        $('#datepicker').datepicker('getFormattedDate')

    );
})
          $('#datepicker1').datepicker();
       $('#datepicker1').on('changeDate', function() {
    $('#my_hidden_input').val(
        $('#datepicker1').datepicker('getFormattedDate')

    );
})
    $('#datepicker2').datepicker();
    $('#datepicker2').on('changeDate', function() {
    $('#my_hidden_input').val(
        $('#datepicker2').datepicker('getFormattedDate')

    );
})

/********************************************************************* */
/**  USER DETAILS PAGE SCRIPT */

// Method to display eat modal with the user name and pizza name
$(document).on('click', '#btnEat', function(){
    let row = $(this).closest('tr');
    let userName= row.find("td:nth-child(1)").text();
    $('#pizzaName').text( userName);
});

// Method to handle post method  to give user a pizza using ajax call

// Method to handle post method  to give user a pizza using ajax call
$(document).on('click', '#btnGivePizza', function(e){
    e.preventDefault();
    $.ajax({
        type:'POST',
        url:'/userdetail',
        dataType: 'json',        
        data:{
            _token : $('meta[name="csrf-token"]').attr('content'),
            first_name : $('#firstNameUd').val(),
            last_name : $('#lastNameUd').val(),
            pizzaId : $('#selectPizzas :selected').val()
        },
        success:function(data){
            $('#add-pizza').modal('hide');
            console.log()
            let location = window.location;
            // redirect to new link in case of a new user added data is just user id
            window.location = window.location.origin + "/userinfo/" + data;
                   
        },
        error:function(xhr, status, err){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            console.log('Error - ' + errorMessage);   
            $('#add-pizza').modal('hide');   
            location.reload();         
        }
    });
});

/****************************************************************************** */
/**************** USER MANAGER PAGE ********************************************* */
/*
*Event to handle the add button click to add a new user using ajax request post
*/
    $(document).on('click', "#saveUser", function(){
        $.ajax({
        type:'POST',
        url:'/users',        
        data:{
            _token : $('meta[name="csrf-token"]').attr('content'),
            first_name : $('#firstName').val(),
            last_name : $('#lastName').val()
        },
        success:function(data){
            $('#add-user').modal('hide');
            location.reload();     
        },
        error:function(xhr, status, err){
            var errorMessage = xhr.status + ': ' + xhr.statusText
            console.log('Error - ' + errorMessage);   
            $('#add-user').modal('hide');            
            }
        });
    });
    //});

/**
* Event attach to the search  button
* Search for a user with either first name or last name
*/
$(document).on('click', '#searchBtn', function( e){
    e.preventDefault();
    //get the user name 
    let userToSearch =  $('#searchUserValue').val();
    //check if the user name is not empty
    if (userToSearch){
        $.ajax({
        type:'GET',
        url:'/search/' + userToSearch,        
        dataType:'json',
        success:function(data){
           // check if the result is not empty
           if(data)
           {
               // String to hold the rows of table body
                let htmlTableRow = "";
               // Clear the table rows
                $('tbody').empty();              
                for(var i = 0; i < data.length; i++){
                    let idUser = data[i].id;
                    let firstName = data[i].first_name;
                    let lastName = data[i].last_name;
                    let dateCreation = data[i].created_at ? data[i].created_at: "" ; // in case of null value, display an empty string
                    htmlTableRow +='<tr>';
                    htmlTableRow +='<td class="d-none">'+ idUser + '</td>';
                    htmlTableRow +='<td style="text-align: left">' + firstName + " "+ lastName + '</td>';
                    htmlTableRow +='<td style="text-align: center">' + dateCreation + '</td>';
                    htmlTableRow +='<td><a href="" style="margin-right: 4vh"><i class="ti-eye"></i> View</a>' + 
                                    '<a href="#" data-toggle="modal" data-target="#delete-user"><i class="ti-trash"></i> Delete <span class="d-none"></span></a> </td>';
                     htmlTableRow+='</tr>';
                   

                } 
                    
                $('tbody').append(htmlTableRow)
           }      
        },
        error:function(xhr, status, err){
            var errorMessage = xhr.status + ': ' + xhr.statusText
            console.log('Error - ' + errorMessage);   
                      
         }
        });
    }   
});

/**
* Event ot display the name of user on modal delete
 */
$(document).on('click', "#deleteUserRef", function(){
  
    $('#displayUserName').html(  $(this).children('span').text())
    $('#deleteUserUri').html($(this).attr('href'))
});
/**
* Event attacled to the delete Button to delete a user from the db
 */
$(document).on('click', '#deleteUser ', function(e){
    // get user id
    let userId = $('#deleteUserUri').html().substr($('#deleteUserUri').html().length -1) //get id from uri delete;

    $.ajax({
    url: $('#deleteUserUri').html(),
    type: 'POST',
    data: { _token: $('meta[name="csrf-token"]').attr('content'), id: userId , _method: 'delete' },
    success:function(){
        // close the modal
        $('#delete-user').modal('hide');
        location.reload();
    },
    error:function(xhr, status, err){
        var errorMessage = xhr.status + ': ' + xhr.statusText
        console.log('Error - ' + errorMessage);   
        $('#delete-user').modal('hide');           
    }
    });
});

/*
* Event to reload the page afer adding a new user
*/
$('#add-user').on('hidden.bs.modal', function (){
    location.reload();
});

})(jQuery);